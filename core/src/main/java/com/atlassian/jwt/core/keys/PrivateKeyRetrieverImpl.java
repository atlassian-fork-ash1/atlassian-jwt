package com.atlassian.jwt.core.keys;


import com.atlassian.jwt.exception.JwtCannotRetrieveKeyException;

import javax.annotation.Nonnull;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.interfaces.RSAPrivateKey;

public class PrivateKeyRetrieverImpl implements PrivateKeyRetriever
{
    private final keyLocationType type;
    private final String location;
    private final KeyUtils keyUtils;

    public PrivateKeyRetrieverImpl(keyLocationType type, String keyLocation)
    {
        this(type, keyLocation, new KeyUtils());
    }

    public PrivateKeyRetrieverImpl(keyLocationType type, String keyLocation, KeyUtils keyUtils)
    {
        this.type = type;
        this.location = keyLocation;
        this.keyUtils = keyUtils;
    }

    @Nonnull
    @Override
    public RSAPrivateKey getPrivateKey() throws JwtCannotRetrieveKeyException
    {
        if (type == keyLocationType.CLASSPATH_RESOURCE)
        {
            return getPrivateKeyFromClasspathResource();
        }
        else if (type == keyLocationType.FILE)
        {
            return getPrivateKeyFromFile();
        }
        else
        {
            throw new JwtCannotRetrieveKeyException("Unsupported key location type " + type);
        }
    }

    private RSAPrivateKey getPrivateKeyFromClasspathResource() throws JwtCannotRetrieveKeyException
    {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(location);

        if (in == null)
        {
            throw new JwtCannotRetrieveKeyException("Could not load classpath resource " + location);
        }

        return keyUtils.readRsaPrivateKeyFromPem(new InputStreamReader(in));
    }

    private RSAPrivateKey getPrivateKeyFromFile() throws JwtCannotRetrieveKeyException
    {
        try (FileReader reader = new FileReader(location))
        {
            return keyUtils.readRsaPrivateKeyFromPem(reader);
        }
        catch (IOException e)
        {
            throw new JwtCannotRetrieveKeyException("Unable to read key file: " + location, e);
        }
    }
}
