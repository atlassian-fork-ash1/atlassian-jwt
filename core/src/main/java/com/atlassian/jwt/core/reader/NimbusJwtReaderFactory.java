package com.atlassian.jwt.core.reader;

import com.atlassian.jwt.SigningAlgorithm;
import com.atlassian.jwt.core.Clock;
import com.atlassian.jwt.core.SimpleJwt;
import com.atlassian.jwt.core.StaticClock;
import com.atlassian.jwt.core.SystemClock;
import com.atlassian.jwt.exception.JwsUnsupportedAlgorithmException;
import com.atlassian.jwt.exception.JwtIssuerLacksSharedSecretException;
import com.atlassian.jwt.exception.JwtParseException;
import com.atlassian.jwt.exception.JwtUnknownIssuerException;
import com.atlassian.jwt.reader.JwtReader;
import com.atlassian.jwt.reader.JwtReaderFactory;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jwt.JWTClaimsSet;

import javax.annotation.Nonnull;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Date;

public class NimbusJwtReaderFactory implements JwtReaderFactory
{
    private final JwtIssuerValidator jwtIssuerValidator;
    private final JwtIssuerSharedSecretService jwtIssuerSharedSecretService;

    public NimbusJwtReaderFactory(JwtIssuerValidator jwtIssuerValidator, JwtIssuerSharedSecretService jwtIssuerSharedSecretService)
    {
        this.jwtIssuerValidator = jwtIssuerValidator;
        this.jwtIssuerSharedSecretService = jwtIssuerSharedSecretService;
    }

    @Nonnull
    @Override
    public JwtReader getReader(@Nonnull String jwt) throws JwtParseException, JwsUnsupportedAlgorithmException, JwtUnknownIssuerException, JwtIssuerLacksSharedSecretException
    {
        return getReader(jwt, SystemClock.getInstance());
    }

    @Nonnull
    @Override
    public JwtReader getReader(@Nonnull String jwt, @Nonnull Date date) throws JwsUnsupportedAlgorithmException, JwtUnknownIssuerException, JwtParseException, JwtIssuerLacksSharedSecretException
    {
        return getReader(jwt, StaticClock.at(date));
    }

    @Nonnull
    @Override
    public JwtReader getReader(@Nonnull String jwt, RSAPublicKey publicKey) throws JwsUnsupportedAlgorithmException, JwtParseException, JwtUnknownIssuerException
    {
        return getReader(jwt, publicKey, SystemClock.getInstance());
    }

    @Nonnull
    @Override
    public JwtReader getReader(@Nonnull String jwt, RSAPublicKey publicKey, @Nonnull Date date) throws JwsUnsupportedAlgorithmException, JwtParseException, JwtUnknownIssuerException
    {
        return getReader(jwt, publicKey, StaticClock.at(date));
    }

    private JwtReader getReader(String jwt, Clock clock) throws JwsUnsupportedAlgorithmException, JwtUnknownIssuerException, JwtParseException, JwtIssuerLacksSharedSecretException
    {
        SimpleUnverifiedJwt unverifiedJwt = new NimbusUnverifiedJwtReader().parse(jwt);
        SigningAlgorithm algorithm = validateAlgorithm(unverifiedJwt);
        String issuer = validateIssuer(unverifiedJwt);

        if (algorithm.requiresSharedSecret())
        {
            return macVerifyingReader(issuer, jwtIssuerSharedSecretService.getSharedSecret(issuer), clock);
        }

        throw new JwsUnsupportedAlgorithmException(String.format("Expected a symmetric signing algorithm such as %s, and not %s. Try a symmetric algorithm.", SigningAlgorithm.HS256, algorithm.name()));
    }

    private JwtReader getReader(String jwt, RSAPublicKey publicKey, Clock clock) throws JwsUnsupportedAlgorithmException, JwtParseException, JwtUnknownIssuerException
    {
        SimpleUnverifiedJwt unverifiedJwt = new NimbusUnverifiedJwtReader().parse(jwt);
        SigningAlgorithm algorithm = validateAlgorithm(unverifiedJwt);
        String issuer = validateIssuer(unverifiedJwt);

        if (algorithm.requiresKeyPair())
        {
            return rsVerifyingReader(issuer, publicKey, clock);
        }

        throw new JwsUnsupportedAlgorithmException(String.format("Expected an asymmetric signing algorithm such as %s, and not %s. Try an asymmetric algorithm.", SigningAlgorithm.RS256, algorithm.name()));
    }

    private JwtReader macVerifyingReader(String issuer, String sharedSecret, Clock clock)
    {
        return new NimbusMacJwtReader(issuer, sharedSecret, clock);
    }

    private JwtReader rsVerifyingReader(String issuer, RSAPublicKey publicKey, Clock clock)
    {
        return new NimbusRsJwtReader(issuer, publicKey, clock);
    }

    private String validateIssuer(SimpleUnverifiedJwt unverifiedJwt) throws JwtUnknownIssuerException
    {
        String issuer = unverifiedJwt.getIssuer();

        if (!jwtIssuerValidator.isValid(issuer))
        {
            throw new JwtUnknownIssuerException(issuer);
        }

        return issuer;
    }

    private SigningAlgorithm validateAlgorithm(SimpleUnverifiedJwt unverifiedJwt) throws JwsUnsupportedAlgorithmException
    {
        return SigningAlgorithm.forName(unverifiedJwt.getAlgorithm());
    }

    private static class SimpleUnverifiedJwt extends SimpleJwt
    {
        private final String algorithm;

        public SimpleUnverifiedJwt(String algorithm, String iss, String sub, String payload)
        {
            super(iss, sub, payload);
            this.algorithm = algorithm;
        }

        public String getAlgorithm()
        {
            return algorithm;
        }
    }

    private static class NimbusUnverifiedJwtReader
    {
        public SimpleUnverifiedJwt parse(String jwt) throws JwtParseException
        {
            JWSObject jwsObject = parseJWSObject(jwt);
            try
            {
                JWTClaimsSet claims = JWTClaimsSet.parse(jwsObject.getPayload().toJSONObject());
                return new SimpleUnverifiedJwt(jwsObject.getHeader().getAlgorithm().getName(), claims.getIssuer(), claims.getSubject(), jwsObject.getPayload().toString());
            }
            catch (ParseException e)
            {
                throw new JwtParseException(e);
            }
        }

        private JWSObject parseJWSObject(String jwt) throws JwtParseException
        {
            JWSObject jwsObject;

            try
            {
                jwsObject = JWSObject.parse(jwt);
            }
            catch (ParseException e)
            {
                throw new JwtParseException(e);
            }
            return jwsObject;
        }
    }
}
