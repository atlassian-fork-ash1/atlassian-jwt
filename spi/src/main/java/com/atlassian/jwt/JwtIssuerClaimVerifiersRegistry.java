package com.atlassian.jwt;

import com.atlassian.jwt.reader.JwtClaimVerifiersBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * SPI to allow issuers to provide their own claim verifiers
 *
 * @since 3.1
 */
public interface JwtIssuerClaimVerifiersRegistry
{
    /**
     * Return a {@link JwtClaimVerifiersBuilder} that will be used _instead_ of the
     * {@code DefaultJwtClaimVerifiersBuilder} for this issuer.
     *
     * @param issuerName the issuer name
     * @return a JwtClaimVerifiersBuilder or {@code null} if the default claim verifiers builder should be used
     */
    @Nullable
    JwtClaimVerifiersBuilder getClaimVerifiersBuilder(@Nonnull String issuerName);
}
