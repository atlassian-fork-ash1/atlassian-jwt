package com.atlassian.jwt.internal;

import com.atlassian.jwt.JwtIssuer;
import com.atlassian.jwt.JwtIssuerClaimVerifiersRegistry;
import com.atlassian.jwt.JwtIssuerRegistry;
import com.atlassian.jwt.exception.JwtIssuerLacksSharedSecretException;
import com.atlassian.jwt.exception.JwtUnknownIssuerException;
import com.atlassian.jwt.reader.JwtClaimVerifiersBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PluginJwtRegistryTest
{
    private static final String ISSUER_NAME = "issuer-name";

    @Mock
    private BundleContext bundleContext;
    @Mock
    private JwtClaimVerifiersBuilder claimVerifiersBuilder;
    @Mock
    private JwtIssuerClaimVerifiersRegistry claimVerifiersRegistry;
    @Mock
    private ServiceReference<JwtIssuerClaimVerifiersRegistry> claimVerifiersRegistryServiceReference;
    @Mock
    private JwtIssuer issuer;
    @Mock
    private JwtIssuerRegistry issuerRegistry1;
    @Mock
    private JwtIssuerRegistry issuerRegistry2;
    @Mock
    private ServiceReference<JwtIssuerRegistry> issuerRegistryServiceReference1;
    @Mock
    private ServiceReference<JwtIssuerRegistry> issuerRegistryServiceReference2;
    private PluginJwtRegistry registry;

    @Before
    public void beforeEachTest() throws InvalidSyntaxException
    {
        when(bundleContext.getServiceReferences(eq(JwtIssuerClaimVerifiersRegistry.class.getName()), any()))
                .thenReturn(new ServiceReference[]{claimVerifiersRegistryServiceReference});
        when(bundleContext.getService(claimVerifiersRegistryServiceReference)).thenReturn(claimVerifiersRegistry);

        when(bundleContext.getServiceReferences(eq(JwtIssuerRegistry.class.getName()), any()))
                .thenReturn(new ServiceReference[]{issuerRegistryServiceReference1, issuerRegistryServiceReference2});
        when(bundleContext.getService(issuerRegistryServiceReference1)).thenReturn(issuerRegistry1);
        when(bundleContext.getService(issuerRegistryServiceReference2)).thenReturn(issuerRegistry2);
        when(issuerRegistry1.getIssuer(ISSUER_NAME)).thenReturn(issuer);

        registry = new PluginJwtRegistry(bundleContext);
    }

    @Test
    public void destroyClosesServiceTracker()
    {
        registry.destroy();

        verify(bundleContext).ungetService(claimVerifiersRegistryServiceReference);
        verify(bundleContext).ungetService(issuerRegistryServiceReference1);
        verify(bundleContext).ungetService(issuerRegistryServiceReference2);
    }

    @Test
    public void getIssuerIteratesOverRegistries()
    {
        String notFoundIssuer = "no-such-issuer";
        // no match found
        assertThat(registry.getIssuer(notFoundIssuer), nullValue());

        verify(issuerRegistry1).getIssuer(notFoundIssuer);
        verify(issuerRegistry2).getIssuer(notFoundIssuer);
    }

    @Test
    public void getClaimVerifiersBuilderReturnsNullIfNoneFound()
    {
        when(claimVerifiersRegistry.getClaimVerifiersBuilder("issuer")).thenReturn(null);

        JwtClaimVerifiersBuilder builder = registry.getClaimVerifiersBuilder("issuer");

        assertThat(builder, is(nullValue()));
        verify(claimVerifiersRegistry).getClaimVerifiersBuilder("issuer");
    }

    @Test
    public void getClaimVerifiersBuilderReturnsBuilder()
    {
        when(claimVerifiersRegistry.getClaimVerifiersBuilder("issuer")).thenReturn(claimVerifiersBuilder);

        JwtClaimVerifiersBuilder builder = registry.getClaimVerifiersBuilder("issuer");

        assertThat(builder, is(sameInstance(claimVerifiersBuilder)));
    }

    @Test
    public void getSharedSecret() throws JwtUnknownIssuerException, JwtIssuerLacksSharedSecretException
    {
        when(issuer.getSharedSecret()).thenReturn("magic");

        assertThat(registry.getSharedSecret(ISSUER_NAME), is("magic"));
    }

    @Test(expected = JwtUnknownIssuerException.class)
    public void getSharedSecretUsingNonExistentIssuerResultsInException() throws JwtUnknownIssuerException, JwtIssuerLacksSharedSecretException
    {
        registry.getSharedSecret("no-such-issuer");
    }

    @Test(expected = NullPointerException.class)
    public void getSharedSecretUsingNullIssuerIdResultsInException() throws JwtUnknownIssuerException, JwtIssuerLacksSharedSecretException
    {
        registry.getSharedSecret(null);
    }

    @Test(expected = JwtIssuerLacksSharedSecretException.class)
    public void getSharedSecretUsingNoAuthAddOnIdResultsInException() throws JwtUnknownIssuerException, JwtIssuerLacksSharedSecretException
    {
        registry.getSharedSecret(ISSUER_NAME);
    }

    @Test
    public void testIsValidReturnsFalseForNonExistentIssuers()
    {
        assertThat(registry.isValid("non-existent"), is(false));
    }

    @Test
    public void testIsValidReturnsFalseForNullIssuer()
    {
        assertThat(registry.isValid(null), is(false));
    }

    @Test
    public void testIsValidReturnsTrueForKnownIssuer()
    {
        assertThat(registry.isValid(ISSUER_NAME), is(true));
    }
}