package com.atlassian.jwt.internal.sal;

import com.atlassian.jwt.CanonicalHttpRequest;
import com.atlassian.jwt.Jwt;
import com.atlassian.jwt.JwtService;
import com.atlassian.jwt.core.http.JavaxJwtRequestExtractor;
import com.atlassian.jwt.core.http.auth.AbstractJwtAuthenticator;
import com.atlassian.jwt.exception.JwtIssuerLacksSharedSecretException;
import com.atlassian.jwt.exception.JwtParseException;
import com.atlassian.jwt.exception.JwtUnknownIssuerException;
import com.atlassian.jwt.exception.JwtVerificationException;
import com.atlassian.jwt.internal.PluginJwtRegistry;
import com.atlassian.jwt.reader.JwtClaimVerifiersBuilder;
import com.atlassian.jwt.reader.JwtReaderFactory;
import com.atlassian.sal.api.auth.Authenticator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static com.atlassian.jwt.JwtConstants.HttpRequests.ADD_ON_ID_ATTRIBUTE_NAME;
import static com.atlassian.jwt.JwtConstants.HttpRequests.JWT_JSON_PAYLOAD_ATTRIBUTE_NAME;
import static com.atlassian.jwt.JwtConstants.HttpRequests.JWT_SUBJECT_ATTRIBUTE_NAME;
import static java.util.Objects.requireNonNull;

/**
 * A JwtAuthenticator for requests associated with an ApplicationLink (i.e. for requests between two linked applications)
 */
public class JwtAuthenticatorImpl extends AbstractJwtAuthenticator<HttpServletRequest, HttpServletResponse, Authenticator.Result>
        implements Authenticator
{
    private final JwtClaimVerifiersBuilder defaultClaimVerifiersBuilder;
    private final JwtReaderFactory jwtReaderFactory;
    private final PluginJwtRegistry jwtRegistry;
    private final JwtService jwtService;

    public JwtAuthenticatorImpl(JwtClaimVerifiersBuilder defaultClaimVerifiersBuilder, JwtReaderFactory jwtReaderFactory, PluginJwtRegistry jwtRegistry, JwtService jwtService)
    {
        super(new JavaxJwtRequestExtractor(), new DefaultAuthenticationResultHandler());

        this.defaultClaimVerifiersBuilder = defaultClaimVerifiersBuilder;
        this.jwtReaderFactory = jwtReaderFactory;
        this.jwtRegistry = jwtRegistry;
        this.jwtService = requireNonNull(jwtService, "jwtService");
    }

    @Override
    protected void tagRequest(HttpServletRequest request, Jwt jwt)
    {
        request.setAttribute(ADD_ON_ID_ATTRIBUTE_NAME, jwt.getIssuer());
        request.setAttribute(JWT_JSON_PAYLOAD_ATTRIBUTE_NAME, jwt.getJsonPayload());
        request.setAttribute(JWT_SUBJECT_ATTRIBUTE_NAME, jwt.getSubject());
    }

    @Override
    protected Jwt verifyJwt(String jwtString, CanonicalHttpRequest canonicalHttpRequest) throws JwtParseException, JwtVerificationException, JwtIssuerLacksSharedSecretException, JwtUnknownIssuerException, IOException, NoSuchAlgorithmException
    {
        Jwt jwt = jwtReaderFactory.getReader(jwtString).readUnverified(jwtString);
        String issuer = jwt.getIssuer();
        JwtClaimVerifiersBuilder claimVerifiersBuilder = getClaimVerifiersBuilder(issuer);
        return jwtService.verifyJwt(jwtString, claimVerifiersBuilder.build(canonicalHttpRequest));
    }

    private JwtClaimVerifiersBuilder getClaimVerifiersBuilder(String issuer)
    {
        if (issuer == null) {
            return defaultClaimVerifiersBuilder;
        }
        JwtClaimVerifiersBuilder builder = jwtRegistry.getClaimVerifiersBuilder(issuer);
        return (builder != null) ? builder : defaultClaimVerifiersBuilder;
    }
}
