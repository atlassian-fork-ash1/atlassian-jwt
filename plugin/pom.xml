<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.jwt</groupId>
        <artifactId>jwt-parent</artifactId>
        <version>3.2.1-SNAPSHOT</version>
    </parent>

    <artifactId>jwt-plugin</artifactId>
    <name>Atlassian JWT Plugin</name>
    <description>
        JWT client and server capabilities packaged as an Atlassian Plugin.
    </description>
    <packaging>atlassian-plugin</packaging>

    <properties>
        <!-- OnDemand compatibility test directives -->
        <od.testing.configuration>
            use-maven,3.3.3
            use-java,8
            include-artifact,com.atlassian.jwt:jwt-integration-tests
        </od.testing.configuration>
    </properties>

    <dependencies>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>jwt-api</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>jwt-applinks</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>jwt-core</artifactId>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>jwt-spi</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.security</groupId>
            <artifactId>atlassian-secure-random</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-host</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.crowd</groupId>
            <artifactId>embedded-crowd-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.mail</groupId>
            <artifactId>mail</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>amps-maven-plugin</artifactId>
                <configuration>
                    <extractDependencies>true</extractDependencies>
                    <skipManifestValidation>true</skipManifestValidation>
                    <jvmArgs>-Xmx1024m -XX:MaxPermSize=256m</jvmArgs>

                    <instructions>
                        <Spring-Context>*</Spring-Context>
                        <Export-Package>
                            com.atlassion.jwt,
                            com.atlassian.jwt.*
                        </Export-Package>
                        <Import-Package>
                            <!-- Import and export the packages provided by jwt-api, jwt-spi and jwt-applinks in case
                                 the host application provides them -->
                            com.atlassian.jwt;resolution:="optional";version="3",
                            com.atlassian.jwt.*;resolution:="optional";version="3",
                            com.atlassian.applinks.*,
                            com.atlassian.sal.api.auth;version="4",
                            com.atlassian.sal.api.lifecycle,
                            com.atlassian.sal.api.message,
                            com.atlassian.sal.api.net,
                            com.atlassian.sal.api.features,
                            com.atlassian.security.random,
                            com.google.common.base,
                            com.google.common.collect,
                            javax.crypto,
                            javax.crypto.interfaces,
                            javax.crypto.spec,
                            javax.mail.internet,
                            javax.naming,
                            javax.naming.directory,
                            javax.net,
                            javax.net.ssl,
                            javax.security.auth.callback,
                            javax.security.auth.x500,
                            javax.security.sasl,
                            javax.servlet,javax.servlet.http,
                            javax.xml.transform,
                            javax.xml.transform.stream,
                            org.apache.avalon.framework.logger;resolution:="optional",
                            org.apache.commons.codec.binary;version="${commons.codec.version}",
                            org.apache.commons.lang3*,
                            org.apache.log;resolution:="optional",
                            org.apache.log4j;resolution:="optional",
                            org.ietf.jgss,
                            org.osgi.*,
                            org.slf4j,
                            sun.security.util;resolution:="optional"
                        </Import-Package>
                        <Private-Package>
                            com.atlassian.jwt.internal*
                        </Private-Package>
                    </instructions>

                    <systemPropertyVariables>
                        <atlassian.mail.senddisabled>false</atlassian.mail.senddisabled>
                        <sun.net.http.allowRestrictedHeaders>true</sun.net.http.allowRestrictedHeaders>
                    </systemPropertyVariables>

                    <versionOverrides>
                        <versionOverride>maven-surefire-plugin</versionOverride>
                    </versionOverrides>

                    <containerId>tomcat85x</containerId>

                    <products>
                        <product>
                            <id>jira</id>
                            <version>${atlassian.jira.version}</version>
                            <systemPropertyVariables>
                                <product>jira</product>
                            </systemPropertyVariables>
                            <pluginArtifacts>
                                <pluginArtifact>
                                    <groupId>${project.groupId}</groupId>
                                    <artifactId>jwt-test-plugin</artifactId>
                                    <version>${project.version}</version>
                                </pluginArtifact>
                            </pluginArtifacts>
                        </product>
                    </products>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>ide</id>
            <!-- Duplicate dependency declarations for all dependencies of type "atlassian-plugin"
               are here to make IDEA/Eclipse happy.  Activate this profile within your IDE only. -->
            <dependencies>
                <dependency>
                    <groupId>${project.groupId}</groupId>
                    <artifactId>jwt-plugin</artifactId>
                    <version>${project.version}</version>
                    <scope>compile</scope>
                </dependency>
            </dependencies>
        </profile>
    </profiles>

</project>
