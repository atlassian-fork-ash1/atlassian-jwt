#!/bin/bash

MAVEN_SETTINGS=$MAVEN_HOME/conf/settings.xml
echo "Messing up with Maven settings:"
echo $MAVEN_SETTINGS

sed -i'back' '/<servers>/ a\
<server><id>maven-atlassian-com</id><username>${atlassian_private_username}</username><password>${atlassian_private_password}</password></server>' $MAVEN_SETTINGS

sed -i'bak' '/<profiles>/ a\
<profile><id>maven-atlassian-com</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>maven-atlassian-com</id><url>https://packages.atlassian.com/maven/repository/internal</url><snapshots>                        <enabled>true</enabled><updatePolicy>always</updatePolicy><checksumPolicy>fail</checksumPolicy></snapshots></repository></repositories><pluginRepositories><pluginRepository><id>maven-atlassian-com</id><url>https://packages.atlassian.com/maven/repository/internal</url></pluginRepository></pluginRepositories></profile>' $MAVEN_SETTINGS

sed -i'bak' '/<profiles>/ a\
<profile><id>atlassian-public</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-public</id><url>https://packages.atlassian.com/maven/repository/public</url></repository></repositories></profile>' $MAVEN_SETTINGS
